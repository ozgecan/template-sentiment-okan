# Sentiment Analysis Template

Sentiment Analysis Template çalıştırıldığın  cümlenin duygularını 0

ile 4 arasında bir puan döndürür. 0 çok kötü, 2 nötr , 4 çok iyi olarak

puanlandırılmıştır.Stanford CoreNLP kütüphanesi ve Scala kullanılmıştır.

## Versions

v0.1.0


## import sample data

```
$ python data/import_eventserver.py --access_key <your_access_key> --file data/train.tsv
```



## Pio 
$ pio build && pio train && pio deploy


​komutu ile verilerin derleme ve alıştırma yapmak için kullanılır.
## Sorgu

![apı.png](https://bitbucket.org/repo/5aLbEg/images/321785107-ap%C4%B1.png)

$ curl -H "Content-Type: application/json" \
-d '{
  "s" : "This movie sucks!"
  }' \
http://localhost:8000/queries.json \
-w %{time_connect}:%{time_starttransfer}:%{time_total}

Analiz çıktısı:{"sentiment":0.8000000001798788}0.005:0.031:0.031

![python.png](https://bitbucket.org/repo/5aLbEg/images/1636313473-python.png)

import predictionio
engine_client = predictionio.EngineClient(url="http://localhost:8000")
print engine_client.send_query({"s" : "I am happy"})